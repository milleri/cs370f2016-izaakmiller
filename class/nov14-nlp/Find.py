import re

text = 'abbaaabbbbaaaaa'

pattern = 'ab'

# findall() function returns all of the substrings of the input that match the pattern without overlapping.
for match in re.findall(pattern, text):
    print 'Found "%s"' % match

#finditer() returns iterator of Match instances

for match in re.findinter(pattern, text):
    s = match.start()
    e = match.end()
    print 'Found "{}" at {}:{}'.format(text[s:e], s, e)

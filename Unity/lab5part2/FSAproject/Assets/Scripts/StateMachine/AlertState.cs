﻿using UnityEngine;
using System.Collections;

public class AlertState : IEnemyState 

{
	private readonly StatePatternEnemy enemy;
	private float searchTimer;
	public int reward;

	public AlertState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		Look ();
		Search ();
	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{
		enemy.currentState = enemy.patrolState;
		searchTimer = 0f;
	}

	public void ToAlertState()
	{
		Debug.Log ("Can't transition to same state");
	}

	public void ToChaseState()
	{
		enemy.currentState = enemy.chaseState;
		searchTimer = 0f;
	}

	private void Look()
	{
		RaycastHit hit;
		if (Physics.Raycast (enemy.eyes.transform.position, enemy.eyes.transform.forward, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player")) {
			enemy.chaseTarget = hit.transform;
			ToChaseState();
		}
	}

	private void Search()
	{
		enemy.meshRendererFlag.material.color = Color.yellow;
		enemy.navMeshAgent.Stop ();
		enemy.transform.Rotate (0, enemy.searchingTurnSpeed * Time.deltaTime, 0);
		searchTimer += Time.deltaTime;
		int rand = Random.Range (1, 101); //Random number between 1 and 100

		if (searchTimer >= enemy.searchingDuration) {
			ToPatrolState ();
		}

		if (rand <= 30) { //Random probability to change states
			ToPatrolState ();
			reward = reward - 3;
		}else if (rand > 30) {
			ToChaseState ();
			reward = reward + 10;
		} else {
			enemy.navMeshAgent.Resume ();
		}
	}

}
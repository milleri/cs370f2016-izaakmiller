﻿using UnityEngine;
using System.Collections;

public class ChaseState : IEnemyState 

{

	private readonly StatePatternEnemy enemy;
	public int reward;


	public ChaseState (StatePatternEnemy statePatternEnemy)
	{
		enemy = statePatternEnemy;
	}

	public void UpdateState()
	{
		Look ();
		Chase ();
	}

	public void OnTriggerEnter (Collider other)
	{

	}

	public void ToPatrolState()
	{

	}

	public void ToAlertState()
	{
		enemy.currentState = enemy.alertState;
	}

	public void ToChaseState()
	{

	}

	private void Look()
	{
		RaycastHit hit;
		Vector3 enemyToTarget = (enemy.chaseTarget.position + enemy.offset) - enemy.eyes.transform.position;
		if (Physics.Raycast (enemy.eyes.transform.position, enemyToTarget, out hit, enemy.sightRange) && hit.collider.CompareTag ("Player")) {
			enemy.chaseTarget = hit.transform;

		}
		else
		{
			ToAlertState();
		}

	}

	private void Chase()
	{
		enemy.meshRendererFlag.material.color = Color.red;
		enemy.navMeshAgent.destination = enemy.chaseTarget.position;
		enemy.navMeshAgent.Resume ();
		int rand = Random.Range (1, 101); //Random number between 1 and 100

		if (rand <= 5) { //Random probability to change states
			ToPatrolState ();
			reward = reward - 5;
		} else if (rand <= 20 && rand > 5) {
			ToAlertState ();
			reward = reward + 4;
		} else {
			enemy.navMeshAgent.Resume ();
		}
	}


}
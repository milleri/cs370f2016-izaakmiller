from nltk.sentiment.vader import SentimentIntensityAnalyzer #import analyzer

f = open('reviews.txt','r') #open file

myList = [] #create empty list
num = 0
posCount = 0
negCount = 0
neuCount = 0
for line in f: #fill list with reviews
    myList.insert(num,line)
    num+1

sid = SentimentIntensityAnalyzer() #create SentimentIntensityAnalyzer
for line in myList: #Loop through and analyze each line
    print line
    ss = sid.polarity_scores(line) #score each line
    for k in sorted(ss):
        print('{0}: {1}, '.format(k, ss[k]))
        if (k == "neg"):
            negValue = ss[k]
        if (k == "neu"):
            neuValue = ss[k]
        if (k == "pos"):
            posValue = ss[k]
            if (posValue > negValue or posValue > neuValue): #counting to see which has the most points
                posCount = posCount+1
            elif (negValue > posValue or negValue > neuValue):
                negCount = negCount+1
            elif (neuValue > negValue or neuValue > posValue):
                neuCount = neuCount+1

print "\nPositive value is {}, negative value is {}, and neutral value is {}.".format(posCount, negCount, neuCount)
f.close()

environment = open('environment.txt', 'r')
lights = False

for line in environment:
    env = line.strip('\n')
    if env == 'light':
        lights = True
    elif str(line) == str('dark'):
        lights = False

if lights == True:
    print 'Lights off!'
else:
    print 'Lights on!'

environment.close()

